<?php

/**
 * @file
 * Contains \Drupal\book\Controller\StyleguideController.
 */

namespace Drupal\styleguide\Controller;

use \Drupal\Core\Config\ConfigFactory;
use \Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use \Drupal\Core\Extension\ModuleHandlerInterface;
use \Drupal\Core\Extension\ThemeHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Controller routines for styleguide routes.
 */
class StyleguideController implements ContainerInjectionInterface {
  /**
    * The config factory.
    *
    * @var Drupal\Core\Config\ConfigFactoryInterface
    */
  protected $config_factory;

  /**
    * The module handler.
    *
    * @var \Drupal\Core\Extension\ModuleHandlerInterface
    */
  protected $module_handler;

  /**
    * The theme handler.
    *
    * @var \Drupal\Core\Extension\ThemeHandlerInterface
    */
  protected $theme_handler;

  /**
   * Constructs a StyleguideController object.
   */
  public function __construct(ConfigFactory $config_factory, ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler) {
    $this->config_factory = $config_factory;
    $this->module_handler = $module_handler;
    $this->theme_handler = $theme_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('theme_handler')
    );
  }

  /**
   * Prints a listing of all books.
   */
  public function styleguideRender($theme) {
    // Get the path to the module for loading includes.
  $path = drupal_get_path('module', 'styleguide');

  // TODO: notice about the Overlay module?

  $theme = null;
  // Check the theme.
  if (is_null($theme)) {
    $theme = $this->config_factory->get('system.theme')->get('default');
  }

  // Get theme data.
  $themes = $this->theme_handler->listInfo();
  $active = $themes[$theme];

  // Include modules for which we implement styleguides.
  $modules = array_keys($this->module_handler->getModuleList());
  $items = array ();
  foreach ($modules as $module) {
    $lib = $path . '/modules/' . $module . '.inc';
    if (file_exists($lib)) {
      $this->module_handler->loadInclude('styleguide', 'inc', '/modules/' . $module);

      // Calls hook manually. These are defined manually.
      $items += (array)call_user_func($module . '_styleguide');
    }
  }

  // Get visual testing elements.
  $items += (array) $this->module_handler->invokeAll('styleguide');
  $this->module_handler->alter('styleguide', $items);

  // Get theme style information.
  $theme_info = $active->info;
  $this->module_handler->alter('styleguide_theme_info', $theme_info, $theme);

  $groups = array();
  foreach ($items as $key => $item) {
    if (!isset($item['group'])) {
      $item['group'] = t('Common');
    }
    else {
      $item['group'] = t('@group', array('@group' => $item['group']));
    }
    $item['title'] = t('@title', array('@title' => $item['title']));
    $groups[$item['group']][$key] = $item;
  }
  ksort($groups);
  // Create a navigation header.

  $header = array();
  $head = '';
  $content = '';
  // Process the elements, by group.
  foreach ($groups as $group => $elements) {
    foreach ($elements as $key => $item) {
      $display = '';
      // Output a standard theme item.

      if (isset($item['theme'])) {
        $display = theme($item['theme'], $item['variables']);
      }
      // Output a standard HTML tag. In Drupal 7, the preference
      // is to pass theme('html_tag') instead. This is kept for API
      // compatibility with Drupal 6.
      elseif (isset($item['tag']) && isset($item['content'])) {
        if (empty($item['attributes'])) {
          $display = '<' . $item['tag'] . '>' . $item['content'] . '</' . $item['tag'] . '>';
        }
        else {
          $display = '<' . $item['tag'] . ' ' . new Attribute($item['attributes']) . '>' . $item['content'] . '</' . $item['tag'] . '>';
        }
      }
      // Support a renderable array for content.
      elseif (isset($item['content']) && is_array($item['content'])) {
        $display = drupal_render($item['content']);
      }
      // Just print the provided content.
      elseif (isset($item['content'])) {
        $display = $item['content'];
      }
      // Add the content.
      $content .= theme('styleguide_item', array('key' => $key, 'item' => $item, 'content' => $display));
      // Prepare the header link.
      $header[$group][] = l($item['title'], current_path(), array('fragment' => $key));
    }
    $head .= theme('item_list', array('items' => $header[$group], 'title' => $group));
  }

  // Return the page.
  $build = array();
  $build['header']['#markup'] = theme('styleguide_header', array('theme_info' => $theme_info));
  $build['navigation']['#markup']= theme('styleguide_links', array('items' => $head));
  $build['content']['#markup'] = theme('styleguide_content', array('content' => $content));
  $build['content']['#attached']['css'][] = $path . '/styleguide.css';

  return $build;
  }
}
